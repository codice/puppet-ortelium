$jetty_home = "/usr/local" # this will create a /opt/jetty-$jetty_version folder and a /opt/jetty linking to it
$jetty_version = "7.4.2.v20110526"

# include jetty


class ortelium::ortelium {

  class {'jetty':
  } ->
  exec { "get ortelium":
    command  =>  "wget https://tools.codice.org/artifacts/content/repositories/snapshots/com/stackframe/ortelium/Ortelium/2.1-SNAPSHOT/Ortelium-2.1-20130506.220701-3.war -O /tmp/ortelium.war ; mv /tmp/ortelium.war /usr/local/jetty/webapps",
    creates  =>  "/usr/local/jetty/webapps/ortelium.war",
  } 
  
}